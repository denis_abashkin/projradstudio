//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "FMU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButLevel1Click(TObject *Sender)
{
	Lev = 1;
	DoContinue();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButLevel2Click(TObject *Sender)
{
	Lev = 2;
	DoContinue();
}
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	DoContinue();
}

void Tfm::DoContinue(){
	laCorrect->Text= Format (L"Right = %d", ARRAYOFCONST ((FCountCorrect)));
	laWrong->Text = Format (L"Wrong = %d", ARRAYOFCONST ((FCountWrong)));

	if (Lev == 1) {
		 xValue1 = Random(20);
		 xValue2 = Random(20);
	}
	if (Lev == 2) {
		xValue1 = Random(1000);
		xValue2 = Random(1000);
	}

	int xSign = (Random(2) == 1) ? 1: -1;
	int xResult = xValue1 + xValue2;
	int xResultNew = (Random(2) == 1) ? xResult : xResult + (Random(7) * xSign);

	FAnswerCorrect = (xResult == xResultNew);
	laCode->Text = Format ("%d + %d =%d",
	ARRAYOFCONST ((xValue1, xValue2, xResultNew)));
}

void Tfm::DoAnswer(bool aValue){
	(aValue == FAnswerCorrect) ?
	FCountCorrect++ : FCountWrong++;
	DoContinue();
}
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	Lev = 1;
	Randomize();
	DoReset();
}

//---------------------------------------------------------------------------
void __fastcall Tfm::ButRestartClick(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButYesClick(TObject *Sender)
{
    DoAnswer(true);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButNoClick(TObject *Sender)
{
	DoAnswer(false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButInfoClick(TObject *Sender)
{
	ShowMessage(L"This Program made by Denis Abashkin");
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
