//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *ButRestart;
	TButton *ButInfo;
	TGridPanelLayout *GridPanelLayout1;
	TGridPanelLayout *GridPanelLayout2;
	TButton *ButYes;
	TButton *ButNo;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *Label3;
	TLabel *laCode;
	TGridPanelLayout *GridPanelLayout3;
	TButton *ButLevel1;
	TButton *ButLevel2;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ButRestartClick(TObject *Sender);
	void __fastcall ButYesClick(TObject *Sender);
	void __fastcall ButNoClick(TObject *Sender);
	void __fastcall ButInfoClick(TObject *Sender);
	void __fastcall ButLevel1Click(TObject *Sender);
	void __fastcall ButLevel2Click(TObject *Sender);
private:	// User declarations
int xValue1;
int xValue2;
int Lev;
int FCountCorrect;
int FCountWrong;
bool FAnswerCorrect;
void DoReset();
void DoContinue();
void DoAnswer(bool aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
