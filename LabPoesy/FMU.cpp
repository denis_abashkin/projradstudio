//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "FMU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
inline int Low(const System::UnicodeString &)
{
#ifdef _DELPHI_STRING_ONE_BASED
return 1;
#else
return 0;
#endif
}
inline int High(const System::UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
return S.Length() - 1;
#endif
}
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	UnicodeString x;
	for (int i = 0; i < MeFull->Lines->Count; i++) {
		  x= MeFull->Lines->Strings[i];
		  if(i%2==1){
				for (int j = Low(x); j <= High(x); j++) {
				if (x[j] !=' ')
				x[j] ='x';

			}
		  }
		  Me1->Lines->Add(x);
	}


bool xFlag;
bool xFlag1;
	for (int i = 0; i  < MeFull->Lines->Count; i++) {
	  x = MeFull->Lines->Strings[i];
	  xFlag = false;
	  for (int j = Low(x); j <= High(x); j++) {
		  if((xFlag) && (x[j] !=' '  ))
			x[j] = 'x';
			if((!xFlag) && (x[j] ==' '  ))
			xFlag = true;
	  }
	  Me2->Lines->Add(x);
	}

	for (int i = 0; i  < MeFull->Lines->Count; i++) {
	  x = MeFull->Lines->Strings[i];
	  xFlag = false;
	  for (int j = Low(x); j <= High(x); j++) {
		  if((xFlag) && (x[j] !=' '  ))
			x[j] = 'x';
			if((!xFlag) && (x[j] !=' '  ))
			xFlag = true;
	  }
	  Me3->Lines->Add(x);
	}


	for (int i = 0; i  < MeFull->Lines->Count; i++) {
	  x = MeFull->Lines->Strings[i];
	  xFlag1 = false;
	  for (int j = High(x); j >= Low(x); j--) {
		  if((xFlag1) && (x[j] !=' '))
			x[j] = 'x';
			if((!xFlag1) && (x[j] ==' '))
			xFlag1 = true;
	  }
	  Me4->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::BuPClick(TObject *Sender)
{
	 MeFull->TextSettings->Font->Size += 1;
	 Me1->TextSettings->Font->Size += 1;
	 Me2->TextSettings->Font->Size += 1;

}
//---------------------------------------------------------------------------
void __fastcall Tfm::BuMClick(TObject *Sender)
{
	MeFull->TextSettings->Font->Size -= 1;
	 Me1->TextSettings->Font->Size -= 1;
	 Me2->TextSettings->Font->Size -= 1;
}
//---------------------------------------------------------------------------
