//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *BuM;
	TButton *BuP;
	TButton *BuAbout;
	TLabel *Label1;
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TMemo *MeFull;
	TMemo *Me1;
	TMemo *Me2;
	TTabItem *TabItem4;
	TMemo *Me3;
	TTabItem *TabItem5;
	TMemo *Me4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall BuPClick(TObject *Sender);
	void __fastcall BuMClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
