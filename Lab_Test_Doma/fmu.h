//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TButton *buAbout;
	TLabel *laCountQ;
	TProgressBar *pb;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiResult;
	TButton *buStart;
	TLabel *laCorrect;
	TMemo *me;
	TLabel *laWrong;
	TImage *Image1;
	TButton *buResult;
	TLabel *Label1;
	TImage *imL;
	TImage *imW;
	TTabItem *ti1;
	TScrollBox *ScrollBox8;
	TLabel *Label7;
	TButton *Button7;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TImage *Image5;
	TTabItem *ti2;
	TScrollBox *ScrollBox9;
	TLabel *Label8;
	TButton *Button12;
	TButton *Button13;
	TButton *Button14;
	TButton *Button15;
	TImage *Image7;
	TTabItem *ti3;
	TScrollBox *ScrollBox1;
	TLabel *Label3;
	TButton *Button1;
	TButton *Button8;
	TButton *Button16;
	TButton *Button17;
	TImage *Image3;
	TTabItem *ti4;
	TScrollBox *ScrollBox2;
	TLabel *Label4;
	TButton *Button18;
	TButton *Button19;
	TButton *Button20;
	TButton *Button21;
	TImage *Image9;
	TTabItem *ti5;
	TScrollBox *ScrollBox3;
	TLabel *Label2;
	TButton *Button2;
	TButton *Button3;
	TButton *Button22;
	TButton *Button23;
	TImage *Image4;
	TTabItem *ti6;
	TScrollBox *ScrollBox4;
	TLabel *Label5;
	TButton *Button4;
	TButton *Button24;
	TButton *Button25;
	TButton *Button26;
	TImage *Image6;
	TTabItem *ti7;
	TScrollBox *ScrollBox5;
	TLabel *Label9;
	TButton *Button5;
	TButton *Button27;
	TButton *Button28;
	TButton *Button29;
	TImage *Image8;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buAllClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buResultClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button19Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button24Click(TObject *Sender);
	void __fastcall Button27Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button17Click(TObject *Sender);
	void __fastcall Button16Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button21Click(TObject *Sender);
	void __fastcall Button20Click(TObject *Sender);
	void __fastcall Button18Click(TObject *Sender);
	void __fastcall Button23Click(TObject *Sender);
	void __fastcall Button22Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button26Click(TObject *Sender);
	void __fastcall Button25Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button28Click(TObject *Sender);
	void __fastcall Button29Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	int FCountCorrect;
    int FCountWrong;
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
