//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	tc->TabPosition = TTabPosition :: None;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	laCountQ->Visible = true;
	me->Lines->Clear();
	FCountCorrect = 0;
	FCountWrong = 0;
	tc->Next();
	pb->Visible = true;
	buAbout->Visible = false;
}
void __fastcall Tfm::buAllClick(TObject *Sender)
{
	UnicodeString x;
//	x = ((TControl *)Sender)->Tag == 1 ? L"�����" : L"�������";
	if (((TControl *)Sender)->Tag == 1)
	{
		x = L"�����";
		FCountCorrect++;
	} else{
		x = L"�������";
		FCountWrong++;
	}
	me->Lines->Add(L"������ " + tc->ActiveTab->Text +" - " + x);
//	me->Lines->Add(Format(L"������ %s - %s, ARRAYOFCONST((tc->ActivTab->TEXT, x"));
	tc->Next(TTabTransition::None);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	pb->Value = tc->ActiveTab->Index;

	if (tc->ActiveTab == tiResult) {
		laCorrect->Text = Format(L"����� = %d %%",
			ARRAYOFCONST ((FCountCorrect*100/(FCountCorrect+FCountWrong) )) );
		 laWrong->Text = Format(L"������� = %d %%",
			ARRAYOFCONST ((FCountWrong*100/(FCountCorrect+FCountWrong) )) );
;
		imW->Visible = (FCountCorrect > FCountWrong);
		imL->Visible = !imW->Visible;
	}

	laCountQ->Text = Format(L"(%d �� %d)",
		ARRAYOFCONST((tc->ActiveTab->Index, tc->TabCount-3)) );
	laCountQ->Visible = (tc->ActiveTab != tiMenu) && (tc->ActiveTab != tiResult);
	pb->Visible = (tc->ActiveTab != tiMenu);


}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage("������� �����");
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall Tfm::buResultClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
	buAbout->Visible = true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button9Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �1 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button13Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �2 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button8Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �3 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}

//---------------------------------------------------------------------------

void __fastcall Tfm::Button19Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �4 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}

//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �5 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button24Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �6 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button27Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �7 ��� ���������� �����");
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button11Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �1 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button10Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �1 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button7Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �1 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button15Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �2 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button14Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �2 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button12Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �2 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button17Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �3 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button16Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �3 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �3 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button21Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �4 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button20Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �4 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button18Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �4 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button23Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �5 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button22Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �5 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �5 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button26Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �6 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button25Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �6 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �6 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button28Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �7 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button29Click(TObject *Sender)
{
    me->Lines->Add(L"�� ������� �7 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	me->Lines->Add(L"�� ������� �7 ��� ������������ �����");
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

