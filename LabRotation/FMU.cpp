//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "FMU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TLabRotation *LabRotation;
//---------------------------------------------------------------------------
__fastcall TLabRotation::TLabRotation(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::But30Click(TObject *Sender)
{
	TrackBar1->Value = 30;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::But60Click(TObject *Sender)
{
	TrackBar1->Value = 60;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::But90Click(TObject *Sender)
{
	TrackBar1->Value = 90;
}
//---------------------------------------------------------------------------

void __fastcall TLabRotation::But120Click(TObject *Sender)
{
	TrackBar1->Value = 120;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::TrackBar1Change(TObject *Sender)
{
	Image1->RotationAngle = TrackBar1->Value;
	laRotationAngel->Text = TrackBar1->Value;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::ButMinus10Click(TObject *Sender)
{
	 TrackBar1->Value -=10;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::ButPlus10Click(TObject *Sender)
{
	  TrackBar1->Value +=10;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::ButHClick(TObject *Sender)
{
	Image1->Bitmap->FlipHorizontal();
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::ButVClick(TObject *Sender)
{
	Image1->Bitmap->FlipVertical();
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::TrackBar2Change(TObject *Sender)
{
	Image1->Opacity = TrackBar2->Value;
}
//---------------------------------------------------------------------------
void __fastcall TLabRotation::TrackBar3Change(TObject *Sender)
{
				 Image1->Height = TrackBar3->Value;
                 Image1->Width = TrackBar3->Value;
}
//---------------------------------------------------------------------------
