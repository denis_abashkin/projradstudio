//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TLabRotation : public TForm
{
__published:	// IDE-managed Components
	TButton *But30;
	TButton *But60;
	TButton *But90;
	TButton *But120;
	TButton *ButPlus10;
	TButton *ButMinus10;
	TImage *Image1;
	TTrackBar *TrackBar1;
	TLabel *laRotationAngel;
	TButton *ButH;
	TButton *ButV;
	TTrackBar *TrackBar2;
	TTrackBar *TrackBar3;
	void __fastcall But30Click(TObject *Sender);
	void __fastcall But60Click(TObject *Sender);
	void __fastcall But90Click(TObject *Sender);
	void __fastcall But120Click(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall ButMinus10Click(TObject *Sender);
	void __fastcall ButPlus10Click(TObject *Sender);
	void __fastcall ButHClick(TObject *Sender);
	void __fastcall ButVClick(TObject *Sender);
	void __fastcall TrackBar2Change(TObject *Sender);
	void __fastcall TrackBar3Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TLabRotation(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLabRotation *LabRotation;
//---------------------------------------------------------------------------
#endif
