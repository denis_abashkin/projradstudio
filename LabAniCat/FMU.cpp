//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "FMU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Image1Click(TObject *Sender)
{
	FCountCat++;
	laCountCat->Text = "����� = " + IntToStr(FCountCat);
	FloatAnimation3->Start();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Image2Click(TObject *Sender)
{
	FCountFox++;
	laCountFox->Text = "������� = " + IntToStr(FCountFox);
    	FloatAnimation4->Start();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
  FCountCat=0;
  FCountFox=0;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormResize(TObject *Sender)
{
		FloatAnimation1->StopValue = this->Width - Image1 ->Width;
        FloatAnimation2->StopValue = this->Width - Image2 ->Width;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FloatAnimation1Process(TObject *Sender)
{
	   if ((Image1->Position->X == FloatAnimation1->StopValue)
	   ||(Image1->Position->X == FloatAnimation1->StartValue)) {
				Image1->Bitmap->FlipHorizontal();
	   }
}
//---------------------------------------------------------------------------


void __fastcall Tfm::FloatAnimation2Process(TObject *Sender)
{
		if ((Image2->Position->X == FloatAnimation2->StopValue)
	   ||(Image2->Position->X == FloatAnimation2->StartValue)) {
				Image2->Bitmap->FlipHorizontal();
	   }
}
//---------------------------------------------------------------------------

