//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TFloatAnimation *FloatAnimation1;
	TImage *Image2;
	TFloatAnimation *FloatAnimation2;
	TLabel *laCountCat;
	TLabel *laCountFox;
	TFloatAnimation *FloatAnimation3;
	TFloatAnimation *FloatAnimation4;
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FloatAnimation1Process(TObject *Sender);
	void __fastcall FloatAnimation2Process(TObject *Sender);
private:	// User declarations
	int FCountCat;
    int FCountFox;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
