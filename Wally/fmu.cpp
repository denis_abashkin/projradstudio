//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;

//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buStartClick(TObject *Sender)
{
	me->Lines->Clear();
	FCountCorrect = 0;
	FCountWrong = 0;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
	tc->TabPosition = TTabPosition::None;
	pb->Max = tc->TabCount-1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BuAboutClick(TObject *Sender)
{
	  ShowMessage("���� ��� Wally? - Abashkin Denis");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	tc->First();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	FCountWrong++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image3Click(TObject *Sender)
{
	tc->Next();
	FCountCorrect++;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image5Click(TObject *Sender)
{
	tc->Next();
	FCountCorrect++;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image7Click(TObject *Sender)
{
	tc->Next();
	FCountCorrect++;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image9Click(TObject *Sender)
{
	tc->Next();
	FCountCorrect++;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image11Click(TObject *Sender)
{
	FCountCorrect++;
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
    imCorrect->Visible = (FCountCorrect > FCountWrong);
	imWrong->Visible =  ! imCorrect ->Visible;
	pb->Value = tc->ActiveTab->Index;
	laCountQ->Text = Format(L"(%d �� %d)", ARRAYOFCONST((tc->ActiveTab->Index, tc->TabCount-2)) );
	laCountQ->Visible = (tc->ActiveTab !=tiMenu) && (tc->ActiveTab != tiResult);
	laCorrect->Text = Format(L"����� ����� = %d", ARRAYOFCONST((FCountCorrect)) );
	laWrong->Text = Format(L"����� ���������� = %d", ARRAYOFCONST((FCountWrong)) );
}
//---------------------------------------------------------------------------

