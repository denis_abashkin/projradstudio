//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *LayoutTop;
	TButton *BuAbout;
	TLabel *laCountQ;
	TLabel *LaCenterTop;
	TProgressBar *pb;
	TTabControl *tc;
	TTabItem *tiMenu;
	TButton *buStart;
	TImage *Image1;
	TTabItem *ti1;
	TScrollBox *ScrollBox1;
	TTabItem *tiResult;
	TButton *buRestart;
	TMemo *me;
	TLabel *laCorrect;
	TLabel *laWrong;
	TImage *imCorrect;
	TImage *imWrong;
	TButton *Button1;
	TImage *Image2;
	TImage *Image3;
	TTabItem *ti2;
	TScrollBox *ScrollBox2;
	TButton *Button2;
	TImage *Image4;
	TImage *Image5;
	TTabItem *ti3;
	TScrollBox *ScrollBox3;
	TButton *Button3;
	TImage *Image6;
	TImage *Image7;
	TTabItem *ti4;
	TScrollBox *ScrollBox4;
	TButton *Button4;
	TImage *Image8;
	TImage *Image9;
	TTabItem *ti5;
	TScrollBox *ScrollBox5;
	TButton *Button5;
	TImage *Image10;
	TImage *Image11;
	TStyleBook *StyleBook1;
	void __fastcall Image3Click(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall BuAboutClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Image5Click(TObject *Sender);
	void __fastcall Image7Click(TObject *Sender);
	void __fastcall Image9Click(TObject *Sender);
	void __fastcall Image11Click(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
    int FCountWrong;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
