//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Secundomerchik.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::BtnStartClick(TObject *Sender)
{
    me->Lines->Clear();
	FTimeStart = Now();
	tm->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::BtnStopClick(TObject *Sender)
{
	tm->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
	//laTime->Text = TimeToStr(Now() - FTimeStart);
	UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now() - FTimeStart);
    laTime->Text = x.Delete(x.Length() - 2, 2);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::CircleClick(TObject *Sender)
{
	me->Lines->Add(laTime->Text);
}
//---------------------------------------------------------------------------
