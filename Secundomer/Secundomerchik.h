//---------------------------------------------------------------------------

#ifndef SecundomerchikH
#define SecundomerchikH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Graphics.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TButton *BtnStart;
	TButton *BtnStop;
	TLabel *laTime;
	TTimer *tm;
	TMemo *me;
	TButton *Circle;
	TBrushObject *Brush1;
	TButton *BtnPause;
	void __fastcall BtnStartClick(TObject *Sender);
	void __fastcall BtnStopClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall CircleClick(TObject *Sender);
private:	// User declarations
	TDateTime FTimeStart;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
