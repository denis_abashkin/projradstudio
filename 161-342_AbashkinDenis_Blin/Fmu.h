//---------------------------------------------------------------------------

#ifndef FmuH
#define FmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TButton *buAbout;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiBucket;
	TTabItem *tiConect;
	TTabItem *tiGame;
	TTabItem *tiUslov;
	TImage *Image1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buBlin;
	TButton *buGame;
	TButton *buBucket;
	TButton *buConect;
	TButton *buUslov;
	TButton *buTema;
	TButton *buContact;
	TStyleBook *StyleBook2;
	TStyleBook *StyleBook1;
	TLabel *laTop;
	TTabItem *tiBlin;
	TButton *buBackToMenu;
	TTabItem *tiContact;
	TMemo *Me1;
	TMemo *Me2;
	TImage *Image2;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Bu1;
	TButton *bu2;
	TButton *Bu4;
	TButton *Bu5;
	TButton *bu3;
	TButton *Bu6;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *tiBlinchik;
	TImage *Image3;
	TTabItem *tiVictor;
	TButton *buVictor;
	TImage *Image4;
	TLabel *laVict;
	TGridPanelLayout *GridPanelLayout4;
	TButton *buFast;
	TButton *buCorz;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label1;
	TTabControl *TabControl1;
	TTabItem *tiPlay;
	TTabItem *tiEnd;
	void __fastcall buTemaClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBlinClick(TObject *Sender);
	void __fastcall buBackToMenuClick(TObject *Sender);
	void __fastcall buBucketClick(TObject *Sender);
	void __fastcall buConectClick(TObject *Sender);
	void __fastcall buGameClick(TObject *Sender);
	void __fastcall buContactClick(TObject *Sender);
	void __fastcall buUslovClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall ListView1Change(TObject *Sender);
	void __fastcall buVictorClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
