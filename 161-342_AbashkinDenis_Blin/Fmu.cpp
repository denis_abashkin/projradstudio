//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Fmu.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buTemaClick(TObject *Sender)
{
		if (fm->StyleBook == StyleBook1) {
		fm->StyleBook = StyleBook2;
	} else {
		fm->StyleBook = StyleBook1;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	buBackToMenu->Visible = (tc->ActiveTab != tiMenu);
	tc->TabPosition = TTabPosition::None;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBlinClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiBlin->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackToMenuClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBucketClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiBucket->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConectClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiConect->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buGameClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiVictor->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buContactClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiContact->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buUslovClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiUslov->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"Abashkin Denis");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	buBackToMenu->Visible = (tc->ActiveTab != tiMenu);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ListView1Change(TObject *Sender)
{
	tc->GotoVisibleTab(tiBlinchik->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buVictorClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiGame->Index);
}
//---------------------------------------------------------------------------
