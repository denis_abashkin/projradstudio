//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "FMU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	buBackToMenu->Visible = (tc->ActiveTab != tiMenu);
}
void __fastcall Tfm::ButTimeClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiTime->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackToMenuClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButDistanceClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiDistance->Index);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall Tfm::ButInforamationClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiInformation->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::butTemperatureClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiTemperature->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::EdTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xSec;
	//обратное действие
	x = StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch(((TEdit*)Sender)->Tag) {
		case 1: xSec = x / 1000; break;
		case 2: xSec = x; break;
		case 3: xSec = x *60; break;
		case 4: xSec = x * 60 * 60; break;
		case 5: xSec = x * 60 * 60 * 24; break;
		case 6: xSec = x*60*60*24*7; break;
	}
	edMs->Text = FloatToStr(xSec*1000);
	edSec->Text = FloatToStr(xSec);
	edMinutes->Text = FloatToStr(xSec/60);
	edHours->Text = FloatToStr(xSec/60/60);
	edDays->Text = FloatToStr(xSec/60/60/24);
	edWeeks->Text = FloatToStr(xSec/60/60/24/7);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::EdDistanceAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xSec;
	//обратное действие
	x = StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch(((TEdit*)Sender)->Tag) {
		case 1: xSec = x / 1000; break;
		case 2: xSec = x; break;
		case 3: xSec = x *60; break;
		case 4: xSec = x * 60 * 60; break;
		case 5: xSec = x * 60 * 60 * 24; break;
		case 6: xSec = x*60*60*24*7; break;
	}
	edMs->Text = FloatToStr(xSec*1000);
	edSec->Text = FloatToStr(xSec);
	edMinutes->Text = FloatToStr(xSec/60);
	edHours->Text = FloatToStr(xSec/60/60);
	edDays->Text = FloatToStr(xSec/60/60/24);
	edWeeks->Text = FloatToStr(xSec/60/60/24/7);
}
//---------------------------------------------------------------------------
