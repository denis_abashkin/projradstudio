//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TButton *ButInfo;
	TButton *buBackToMenu;
	TLabel *���������;
	TTabItem *tiTime;
	TTabItem *tiDistance;
	TTabItem *tiInformation;
	TTabItem *tiTemperature;
	TGridPanelLayout *GridPanelLayout1;
	TButton *ButTime;
	TButton *ButDistance;
	TButton *ButInforamation;
	TButton *butTemperature;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TListBoxItem *ListBoxItem6;
	TEdit *edMs;
	TEdit *edSec;
	TEdit *edMinutes;
	TEdit *edHours;
	TEdit *edDays;
	TEdit *edWeeks;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem7;
	TEdit *Edit1;
	TListBoxItem *ListBoxItem8;
	TEdit *Edit2;
	TListBoxItem *ListBoxItem9;
	TEdit *Edit3;
	TListBoxItem *ListBoxItem10;
	TEdit *Edit4;
	TListBoxItem *ListBoxItem11;
	TEdit *Edit5;
	TListBoxItem *ListBoxItem12;
	TEdit *Edit6;
	void __fastcall ButTimeClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackToMenuClick(TObject *Sender);
	void __fastcall ButDistanceClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall ButInforamationClick(TObject *Sender);
	void __fastcall butTemperatureClick(TObject *Sender);
	void __fastcall EdTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall EdDistanceAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
