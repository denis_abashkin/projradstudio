//---------------------------------------------------------------------------

#ifndef FmuH
#define FmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *buInfo;
	TProgressBar *pb;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *���������;
	TLabel *lya4et;
	TCheckBox *CheckBox1;
	TCheckBox *CheckBox2;
	TCheckBox *CheckBox3;
	TCheckBox *CheckBox4;
	TCheckBox *CheckBox5;
	TButton *buNext1;
	TButton *BuStart;
	TButton *buNext2;
	TCheckBox *CheckBox6;
	TCheckBox *CheckBox7;
	TCheckBox *CheckBox8;
	TCheckBox *CheckBox9;
	TLabel *Label2;
	TButton *buNext3;
	TCheckBox *CheckBox10;
	TCheckBox *CheckBox11;
	TCheckBox *CheckBox12;
	TCheckBox *CheckBox13;
	TLabel *Label3;
	TCheckBox *CheckBox14;
	TCheckBox *CheckBox15;
	TImage *Image1;
	TLabel *laRight;
	TLabel *laWrong;
	TButton *buRestart;
	TMemo *Memo1;
	void __fastcall buNext1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall BuStartClick(TObject *Sender);
private:	// User declarations
	int FCountCorect;
	int FCountWrong;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
