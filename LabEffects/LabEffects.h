//---------------------------------------------------------------------------

#ifndef LabEffectsH
#define LabEffectsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TShadowEffect *ShadowEffect1;
	TBlurEffect *BlurEffect1;
	TBandsEffect *BandsEffect1;
	TWaveEffect *WaveEffect1;
	TEmbossEffect *EmbossEffect1;
	TMonochromeEffect *MonochromeEffect1;
	TFloatAnimation *FloatAnimation1;
	TButton *Button1;
	TGlowEffect *GlowEffect1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
