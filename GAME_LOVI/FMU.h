//---------------------------------------------------------------------------

#ifndef FMUH
#define FMUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.TabControl.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *Image_ananas;
	TFloatAnimation *FloatAnimation1;
	TImage *Image_apple;
	TFloatAnimation *FloatAnimation2;
	TImage *Image_pear;
	TFloatAnimation *FloatAnimation3;
	TLabel *laTAB;
	TFloatAnimation *FloatAnimation4;
	TTabControl *tc;
	TTabItem *tiMenu;
	TButton *buStart;
	TTabItem *ti1;
	TToolBar *tb;
	TButton *buAbout;
	TLabel *laCountQ;
	TButton *buDopl;
	TLabel *Label1;
	TImage *Image1;
	TImage *Image2;
	TTabItem *ti2;
	TImage *Image_fon;
	TImage *ImageAnanas2;
	TFloatAnimation *FloatAnimation5;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Image_ananasClick(TObject *Sender);
	void __fastcall Image_appleClick(TObject *Sender);
	void __fastcall Image_pearClick(TObject *Sender);
private:	// User declarations
	int FCountAllFruts;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
