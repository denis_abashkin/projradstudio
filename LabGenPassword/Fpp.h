//---------------------------------------------------------------------------

#ifndef FppH
#define FppH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *ly;
	TEdit *edPassword;
	TButton *buPassword;
	TCheckBox *ckLower;
	TCheckBox *ckNumber;
	TCheckBox *ckSpec;
	TCheckBox *ckUpper;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *buInfo;
	TLabel *laLength;
	TNumberBox *edLenght;
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buPasswordClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
