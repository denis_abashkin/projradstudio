//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
UnicodeString RandomStr (int edLenght, bool aLower, bool aUpper, bool aNumber, bool aSpec){
	const char *c1 ="abcdefghijklmnopqrstuvwxyz";
	const char *c2 ="0123456789";
	const char *c3 ="[]{}<>,.;:-+#";
	//
	UnicodeString x = "";
	UnicodeString xResult = "";
	if (aLower)x += c1;
	if (aUpper)x += UpperCase(c1);
	if (aNumber)x += c2;
	if (aSpec)x += c3;
	if (x.IsEmpty())x = c1;
	//
	while (xResult.Length() < edLenght) {
		xResult += x.SubString(Random(x.Length()+1),1);
	}
	return xResult;

}
