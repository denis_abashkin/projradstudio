//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Fpp.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buInfoClick(TObject *Sender)
{
    ShowMessage("LabGenPassword - Denis Abashkin");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPasswordClick(TObject *Sender)
{
	edPassword->Text = RandomStr(StrToIntDef(edLenght->Text,9),
	ckLower->IsChecked, ckUpper->IsChecked, ckNumber->IsChecked, ckSpec->IsChecked);
}
//---------------------------------------------------------------------------
